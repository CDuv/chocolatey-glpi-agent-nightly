﻿VERIFICATION
Verification is intended to assist the Chocolatey moderators and community
in verifying that this package's contents are trustworthy.

The "{{PackageName}}" package embeds "{{SoftwareTitle}}" and it's content can be
verified by following theses steps:

1. Go to URL {{ReleasesUrl}}
2. Find the section for version "{{OfficialVersion}}"
3. Download the MSI Windows installers:
   * For 64 bits architecture: {{DownloadUrlx64}}
   * For 32 bits architecture: {{DownloadUrl}}
4. Compute and check checksum for each MSI file:
   * For 64 bits architecture, the {{ChecksumTypex64}} checksum should be: {{Checksumx64}}
   * For 32 bits architecture, the {{ChecksumType}} checksum should be: {{Checksum}}

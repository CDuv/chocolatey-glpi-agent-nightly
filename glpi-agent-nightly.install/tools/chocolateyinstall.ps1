﻿$ErrorActionPreference = 'Stop' # stop on all errors

$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
  packageName = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType = 'MSI'
  url = 'https://nightly.glpi-project.org/glpi-agent/GLPI-Agent-1.7-git69b99765-x86.msi' # (From $Latest)
  file64 = 'GLPI-Agent-1.7-git69b99765-x64.msi' # (From $Latest)

  softwareName = 'GLPI Agent *'

  checksum = '5E586447AEAEEB2E72A4B568E52F72318F65F3CE8D2036CB557D4630F72B0A03' # (From $Latest)
  checksumType = 'sha256' # (From $Latest)
  checksum64 = '51159D7025A09E4868EFD2967DA7F84515713EC659D19DE5586179EE2188E624' # (From $Latest)
  checksumType64 = 'sha256' # (From $Latest)

  # MSI
  silentArgs = '/quiet'
  validExitCodes= @(0)
}

If ((Get-OSArchitectureWidth 32) -or $env:ChocolateyForceX86 -eq 'true') {
  # Download the 32-bit installer on-the-fly
  Install-ChocolateyPackage @packageArgs
} Else {
  # Use the embedded 64-bit installer

  # Access file from $toolsDir
  $packageArgs.file64 = Join-Path $toolsDir $packageArgs.file64
  Install-ChocolateyInstallPackage @packageArgs
}

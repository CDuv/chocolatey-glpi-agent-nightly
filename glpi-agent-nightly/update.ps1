﻿<#
  .SYNOPSIS
  Updates the glpi-agent-nightly Chocolatey package.

  .DESCRIPTION
  The update.ps1 script fetches information about the latest GLPI Agent Nightly
  build and updates the .nuspec file of the Chocolatey package accordingly (if
  needed). In such case it builds the .nupkg (`choco pack`).
  You can alter script's behavior by setting:
  * $au_Force to $true to update/package no matter what the script fetched.
  * $au_WhatIf to $true to only simulate changes.
  * $au_SkipBuilds to the number of builds the script should skip (from the
    latest). Defaults to 0.

  .INPUTS
  None. You cannot pipe objects to update.ps1.

  .OUTPUTS
  None. update.ps1 does not generate any output.

  .EXAMPLE
  PS> .\update.ps1

  .EXAMPLE
  PS> $au_WhatIf = $true; .\update.ps1
  # Simulate changes.

  .EXAMPLE
  PS> $au_Force = $au_WhatIf = $true; .\update.ps1
  # Simulate and force changes.
#>

Import-Module au

# Load some global variables
. '..\src\globals.ps1'

# Load functions
. '..\src\functions.ps1'

$softwareTitle = 'GLPI Agent Nightly'
$packageVariantPostfix = ''

<#
  .SYNOPSIS
  Perform pre-update tasks.

  .DESCRIPTION
  Don't do anything (no download, no checksum computation).
#>
Function global:au_BeforeUpdate {
  # Do not download anything
}

<#
  .SYNOPSIS
  Populates $Latest with data taken from the "Nightly Builds" website.

  .DESCRIPTION
  Fetches LICENCE from repository.
  Fetches/scraps assets' URL and version/date from "Nightly Builds" website.
#>
Function global:au_GetLatest {
  # Fetch the "Nightly Builds" page
  $buildsPage = Invoke-WebRequest -Uri $global:buildsUrl -UseBasicParsing

  # Was the script asked to skip builds? $au_SkipBuilds variable set?
  If (-Not (Test-Path variable:global:au_SkipBuilds)) {
    $au_SkipBuilds = 0
  } Else {
    Write-Information -MessageData "Skipping $($au_SkipBuilds) builds…" -InformationAction Continue
  }

  # Get the links for the last installer assets
  $downloadLinkPaths = Get-LastBuiltAssetLinks $buildsPage 'msi' $au_SkipBuilds

  # Extract version string from the link's href
  $officialVersion = Get-OfficialVersionFromAssets $downloadLinkPaths

  # Extract build date and time
  $buildDateAndTime = Get-BuildDateTimeForVersion $buildsPage $officialVersion

  # Expand obtained hrefs to full URLs
  $downloadUrls = $downloadLinkPaths | Foreach-Object { Get-ExpandedHrefOfUri -Href $_.href -Uri ([System.Uri]$global:buildsUrl) | Select-Object -ExpandProperty AbsoluteUri }

  # We need a SemVer compliant version for the Chocolatey Automatic Package
  # Updater Module to correctly detect new versions
  $semverVersion = Get-CompatibleSemVer $officialVersion $buildDateAndTime.Date $buildDateAndTime.Time

  # We need a SemVer compliant version without prerelease part for the Chocolatey
  # Community Repository to think it's a stable release (not a prerelease)
  $semverVersionNotPrelease = Get-CompatibleSemVer -NotPrelease $officialVersion $buildDateAndTime.Date $buildDateAndTime.Time

  # Computes raw URL for the licence and fetch it
  $licenceUrls = Get-LicenceUrlsForGitReference $global:licenceUrl (Get-GitCommitId $semverVersion)
  $licencePage = Invoke-WebRequest -Uri $licenceUrls.Raw -UseBasicParsing

  # Load the .nuspec-ext file to get the "true" semver version
  $nuExt = Load-XmlFile (".\$($Latest.PackageName).nuspec-ext" | Convert-Path)

  # Population for $Latest
  @{
    Version = $semverVersion

    # Force the "NuspecVersion" to make Update-Package's version comparison work
    # by using the semver
    NuspecVersion = $nuExt.package.metadata.semver

    PackageVersion = $semverVersionNotPrelease
    OfficialVersion = $officialVersion
    BuildDate = $buildDateAndTime.Date
    BuildTime = $buildDateAndTime.Time
    LicenceUrl = $licenceUrls.Pretty
  }
}

<#
  .SYNOPSIS
  Update configuration files with latest values.

  .DESCRIPTION
  Uses $Latest to alter/update .nuspec files.
#>
Function global:au_SearchReplace {
  @{
    "$($Latest.PackageName).nuspec" = @{
      '(\<title\>).*?(\</title\>)' = "`${1}$($softwareTitle) v$($Latest.OfficialVersion)$($packageVariantPostfix)`${2}"
      '(\<version\>).*?(\</version\>)' = "`${1}$($Latest.PackageVersion)`${2}"
      '(\<licenseUrl\>).*?(\</licenseUrl\>)' = "`${1}$($Latest.LicenceUrl)`${2}"
      '(\<releaseNotes\>).*?(\</releaseNotes\>)' = "`${1}<![CDATA[This v$($Latest.OfficialVersion) nightly build was built on $($Latest.BuildDate) $($Latest.BuildTime) UTC.]]>`${2}"
      "(\<dependency id=`"$($Latest.PackageName)\.install`" version=`")[^`"]*?(`")" = "`${1}$($Latest.PackageVersion)`${2}"
    }

    # Update our .nuspec-ext file:
    "$($Latest.PackageName).nuspec-ext" = @{
      '(\<version\>).*?(\</version\>)' = "`${1}$($Latest.PackageVersion)`${2}"
      '(\<semver\>).*?(\</semver\>)' = "`${1}$($Latest.Version)`${2}"
      '(\<softwareOfficialVersion\>).*?(\</softwareOfficialVersion\>)' = "`${1}$($Latest.OfficialVersion)`${2}"
    }
  }
}

# Starts the actual update of the package but do not automatically compute
# checksum nor update the README.
Update-Package -ChecksumFor none -NoReadme

How to contribute to glpi-agent-nightly *Chocolatey* packages
=============================================================

Declare an issue
----------------

A bug tracker is available on GitLab: https://gitlab.com/CDuv/chocolatey-glpi-agent-nightly/-/issues

Contribute to the code
----------------------

### Version control

The source code is tracked by Git on a GitLab instance at https://gitlab.com/CDuv/chocolatey-glpi-agent-nightly/

### Commits

Commits must follow the [*Conventional Commits* specifications](https://www.conventionalcommits.org/en/v1.0.0/)

### Standards

The `.ps1` files can be checked with [PSScriptAnalyzer](https://github.com/PowerShell/PSScriptAnalyzer).

The `.nuspec` files can be checked with: [choco-nuspec-checker](https://community.chocolatey.org/packages/choco-nuspec-checker).
﻿<#
  .SYNOPSIS
  Some common variables (for global scope) for the packaging of the
  glpi-agent-nightly Chocolatey packages.

  .DESCRIPTION
  This script contains various variables used in the scripts that creates
  the glpi-agent-nightly Chocolatey packages.
#>

# The URL to fetch the LICENCE file from:
$global:licenceUrl = 'https://github.com/glpi-project/glpi-agent/blob/develop/LICENSE'

# The URL to fetch builds informations from:
$global:buildsUrl = 'https://nightly.glpi-project.org/glpi-agent/'

# The checksum algorithm to use on fetched assets:
$global:checksumAlgorithm = 'sha256'

﻿<#
  .SYNOPSIS
  Some functions for the packaging of the glpi-agent-nightly Chocolatey packages.

  .DESCRIPTION
  This script contains various utility functions used in the scripts that creates
  the glpi-agent-nightly Chocolatey packages.
#>

<#
  .SYNOPSIS
  Expands/compute the final URL from a given HTML <a>'s href attribute and the
  URL it's coming from.

  .DESCRIPTION
  In an HTML web page, the href of a link (<a>) can be an URL or a path which
  may be relative or absolute.
  This functions transforms a given href-URL pair into an URL applying basic
  rules:
  * Does href looks like an URL?
  * Does Href starts/ends with a "/"?
  * …

  .PARAMETER Href
  Specifies the value of the HTML <a>'s href attribute.

  .PARAMETER Uri
  Specifies the URL the <a>'s href was used.

  .INPUTS
  None. You cannot pipe objects to Get-ExpandedHrefOfUri.

  .OUTPUTS
  System.Uri. Get-ExpandedHrefOfUri returns an URL where the Href was expanded.

  .EXAMPLE
  # Uri is an URL to a file. Href is also an URL.
  PS> Get-ExpandedHrefOfUri -Href 'https://www.example.net/some/url' -Uri ([System.Uri]'https://www.example.com/1970/01/how-to-expand-href.html')
  # 'https://www.example.net/some/url'

  .EXAMPLE
  # Uri is an URL to a file. Href is an absolute path.
  PS> Get-ExpandedHrefOfUri -Href '/some/absolute_path' -Uri ([System.Uri]'https://www.example.com/1970/01/how-to-expand-href.html')
  # 'https://www.example.com/some/absolute_path'

  .EXAMPLE
  # Uri is an URL to a file. Href is a relative path.
  PS> Get-ExpandedHrefOfUri -Href 'some/relative_path' -Uri ([System.Uri]'https://www.example.com/1970/01/how-to-expand-href.html')
  # 'https://www.example.com/1970/01/some/relative_path'

  .EXAMPLE
  # Uri is an URL to a directory. Href is also an URL.
  PS> Get-ExpandedHrefOfUri -Href 'https://www.example.net/some/url' -Uri ([System.Uri]'https://www.example.com/1970/01/how-to-expand-href/')
  # 'https://www.example.net/some/url'

  .EXAMPLE
  # Uri is an URL to a directory. Href is an absolute path.
  PS> Get-ExpandedHrefOfUri -Href '/some/absolute_path' -Uri ([System.Uri]'https://www.example.com/1970/01/how-to-expand-href/')
  # 'https://www.example.com/some/absolute_path'

  .EXAMPLE
  # Uri is an URL to a directory. Href is a relative path.
  PS> Get-ExpandedHrefOfUri -Href 'some/relative_path' -Uri ([System.Uri]'https://www.example.com/1970/01/how-to-expand-href/')
  # 'https://www.example.com/1970/01/how-to-expand-href/some/relative_path'
#>
Function Get-ExpandedHrefOfUri () {
  [OutputType([System.Uri])]
  Param(
    [Parameter(Mandatory=$true)] [System.String] $Href,
    [Parameter(Mandatory=$true)] [System.Uri] $Uri
  )
  Process {
    If ($Href.StartsWith('http://') -Or $Href.StartsWith('https://')) {
      # No merge
      Return [System.Uri]$Href
    }
    If ($Href.StartsWith('/')) {
      # Replace path
      Return [System.Uri]($Uri.GetLeftPart([System.UriPartial]::Authority) + $Href)
    } ElseIf ($Uri.AbsoluteUri.EndsWith('/')) {
      # Append path
      Return [System.Uri]($Uri.AbsoluteUri + $Href)
    } Else {
      # Append path to URI's parent directory
      Return [System.Uri]($Uri.GetLeftPart([System.UriPartial]::Authority) + ($Uri.Segments[0..($Uri.Segments.Count-2)] -Join '') + $Href)
    }
  }
}

<#
  .SYNOPSIS
  Get the SemVer-compliant version for the Chocolatey Automatic Package Updater
  Module to correctly handles newly discovered versions.

  .DESCRIPTION
  The official GLPI Agent version format is not supported by the NuGet, Chocolatey
  and the Chocolatey Automatic Package Updater Module so newer version could not
  be detected as "newer" than the one in the previous package/nuspec. This
  functions builds a string compatible for version handling.

  .PARAMETER OfficialVersion
  Specifies the official GLPI Agent version.

  .PARAMETER BuildDate
  Specifies the date (format: YYYY-MM-DD) of the build.

  .PARAMETER BuildTime
  Specifies the time (format: hh:mm:ss) of the build.

  .PARAMETER NotPrelease
  Specifies is the returned version should avoid being a prelease.

  .INPUTS
  None. You cannot pipe objects to Get-CompatibleSemVer.

  .OUTPUTS
  System.String. Get-CompatibleSemVer returns the SemVer version as a string.

  .EXAMPLE
  PS> Get-CompatibleSemVer '1.5-git6b8b966f' '2023-04-29' '02:22:16'
  # 1.5-nightly-20230429-022216+git6b8b966f

  .EXAMPLE
  PS> Get-CompatibleSemVer -NotPrelease '1.5-git6b8b966f' '2023-04-29' '02:22:16'
  # 1.5.20230429
#>
Function Get-CompatibleSemVer () {
  [OutputType([System.String])]
  Param(
    [Parameter(Mandatory=$true, Position=0)] [System.String] $OfficialVersion,
    [Parameter(Mandatory=$true, Position=1)] [System.String] $BuildDate,
    [Parameter(Mandatory=$true, Position=2)] [System.String] $BuildTime,
    [Switch] $NotPrelease
  )
  Process {
    $versionParts = $OfficialVersion.Split('-')
    $semverBuildMetadata = $versionParts[1..($versionParts.Count-1)]

    If ($NotPrelease) {
      # Merge…:
      # * real/official version string: x.y.z
      # * with the date: .YYYYMMDD-HHMMSS
      Return $versionParts[0] + '.' + $BuildDate.replace('-', '')
    } Else {
      # Merge…:
      # * real/official version string: x.y.z
      # * with a prerelease part: -nightly-YYYYMMDD-HHMMSS
      # * and build metadata: +gitXXXXXXXX
      Return $versionParts[0] + '-nightly-' + $BuildDate.replace('-', '') + '-' + $BuildTime.replace(':', '') + '+' + $semverBuildMetadata
    }
  }
}

<#
  .SYNOPSIS
  Get the git commit ID/hash for a SemVer version.

  .PARAMETER SemVer
  Specifies the SemVer version.

  .INPUTS
  None. You cannot pipe objects to Get-GitCommitId.

  .OUTPUTS
  System.String. Get-GitCommitId returns commit ID/hash as a string.

  .EXAMPLE
  PS> Get-GitCommitId '1.5-nightly-20230429-022216+git6b8b966f'
  # 6b8b966f
#>
Function Get-GitCommitId () {
  [OutputType([System.String])]
  Param(
    [Parameter(Mandatory=$true, Position=0)] [System.String] $SemVer
  )
  Process {
    # Get Commit ID/hash
    Return $SemVer -replace '^.*\+git([a-z0-9]+)([-_\.])?$', '${1}'
  }
}

<#
  .SYNOPSIS
  Get the last build's assets from the builds page.

  .DESCRIPTION
  The official GLPI Agent builds page lists the 3 latest builds and, for
  each one, the Windows, MacOSX, Linux installers/packages and the source
  tarball.

  For Windows, each build has, .MSI installer and .ZIP archive (containing a
  portable version of the Agent) for x64 and x86 architectures (= 4 assets by
  build).

  .PARAMETER Page
  Specifies the "builds" web page.

  .PARAMETER FileExtension
  Specifies the file extensions we look for ("msi" or "zip").

  .PARAMETER NumberOfBuildsToSkip
  Specifies the number of builds to skip (from the latest). Defauls to 0

  .INPUTS
  None. You cannot pipe objects to Get-LastBuiltAssetLinks.

  .OUTPUTS
  System.Array. Get-LastBuiltAssetLinks returns found links as an array.

  .EXAMPLE
  PS> Get-LastBuiltAssetLinks $page 'msi'
  # An array of links with the following "href" property value:
  # […] /glpi-agent/GLPI-Agent-1.5-git6b8b966f-x64.msi
  # […] /glpi-agent/GLPI-Agent-1.5-git6b8b966f-x86.msi
#>
Function Get-LastBuiltAssetLinks () {
  [OutputType([System.Array])]
  Param(
    [Parameter(Mandatory=$true, Position=0)] [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] $Page,
    [Parameter(Mandatory=$true, Position=1)] [System.String] $FileExtension,
    [Parameter(Mandatory=$false, Position=2)] [System.Int32] $NumberOfBuildsToSkip = 0
  )
  Process {
    # Each build has x assets for a given extension
    $assetPerExtensionPerBuilds = 2;

    Return $Page.Links | Where { $_.href -match ('/GLPI-Agent-.*-x(64|86)\.' + $FileExtension + '$') } | Select-Object -Skip ($NumberOfBuildsToSkip * $assetPerExtensionPerBuilds) -First $assetPerExtensionPerBuilds
  }
}

<#
  .SYNOPSIS
  Get the version from an array links to builds asset (will only use the first
  one).

  .PARAMETER Page
  Specifies the "builds" web page.

  .PARAMETER FileExtension
  Specifies the file extensions we look for ("msi" or "zip").

  .INPUTS
  None. You cannot pipe objects to Get-OfficialVersionFromAssets.

  .OUTPUTS
  System.String. Get-OfficialVersionFromAssets returns commit ID/hash as a string.

  .EXAMPLE
  # $links is an array containing the links for the following "href" property value:
  # * '/glpi-agent/GLPI-Agent-1.5-git6b8b966f-x64.msi'
  # * '/glpi-agent/GLPI-Agent-1.5-git6b8b966f-x86.msi'
  # * '/glpi-agent/GLPI-Agent-1.5-git00000000-x86.msi'
  PS> Get-OfficialVersionFromAssets $links
  # 6b8b966f
#>
Function Get-OfficialVersionFromAssets () {
  [OutputType([System.String])]
  Param(
    [Parameter(Mandatory=$true, Position=0)] [System.Array] $AssetLinks
  )
  Process {
    Return $AssetLinks | Select-Object -ExpandProperty href -First 1 | Select-String -Pattern '/GLPI-Agent-(.*)-x(?:64|86)\..*$' | ForEach-Object {
      $_.Matches[0].Groups[1].Value
    }
  }
}

<#
  .SYNOPSIS
  Get the date and time (as strings) assets of a given build (version) was built.

  .PARAMETER Page
  Specifies the "builds" web page.

  .PARAMETER OfficialVersion
  Specifies the official version we look for (e.g. "1.5-git6b8b966f").

  .INPUTS
  None. You cannot pipe objects to Get-OfficialVersionFromAssets.

  .OUTPUTS
  System.String. Get-OfficialVersionFromAssets returns commit ID/hash as a string.

  .EXAMPLE
  PS> Get-BuildDateTimeForVersion $page '1.5-git6b8b966f'
  # Name      Value
  # ----      -----
  # BuildDate 2023-04-29
  # BuildTime 02:22:16
#>
Function Get-BuildDateTimeForVersion () {
  [OutputType([System.String])]
  Param(
    [Parameter(Mandatory=$true, Position=0)] [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] $Page,
    [Parameter(Mandatory=$true, Position=1)] [System.String] $OfficialVersion
  )
  Process {
    $Page.Content | Select-String -Pattern ('<h1 id="[^"]*">GLPI-Agent v' + $OfficialVersion + ' .*</h1>\s+<p>Built on (\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2}) UTC</p>\s+<h2') | ForEach-Object {
      $buildDate = $_.Matches[0].Groups[1].Value
      $buildTime = $_.Matches[0].Groups[2].Value
    }

    Return @{
      Date = $buildDate
      Time = $buildTime
    }
  }
}

<#
  .SYNOPSIS
  Get the URLs (pretty and raw) of the licence for a given git reference.

  .PARAMETER LicenceUrl
  Specifies the URL to the licence.

  .PARAMETER GitReference
  Specifies the git reference version we look for (e.g. "develop", "6b8b966f").

  .INPUTS
  None. You cannot pipe objects to Get-LicenceUrlsForGitReference.

  .OUTPUTS
  System.Array. Get-LicenceUrlsForGitReference returns URLs as an array.

  .EXAMPLE
  PS> Get-LicenceUrlsForGitReference 'https://github.com/glpi-project/glpi-agent/blob/develop/LICENSE' '6b8b966f'
  # Name   Value
  # ----   -----
  # Pretty https://github.com/glpi-project/glpi-agent/blob/6b8b966f/LICENSE
  # Raw    https://github.com/glpi-project/glpi-agent/raw/6b8b966f/LICENSE
#>
Function Get-LicenceUrlsForGitReference () {
  [OutputType([System.Array])]
  Param(
    [Parameter(Mandatory=$true, Position=0)] [System.String] $LicenceUrl,
    [Parameter(Mandatory=$true, Position=1)] [System.String] $GitReference
  )
  Process {
    $LicenceUrl = $LicenceUrl -replace '^(https://([^/]+/){4})[^/]+/', "`${1}$GitReference/"
    $prettyLicenceUrl = $LicenceUrl -replace '^(https://([^/]+/){3})[^/]+/', '${1}blob/'
    $rawLicenceUrl = $LicenceUrl -replace '^(https://([^/]+/){3})[^/]+/', '${1}raw/'

    Return @{
      Pretty = $prettyLicenceUrl
      Raw = $rawLicenceUrl
    }
  }
}

<#
  .SYNOPSIS
  Loads an XML file as a XmlDocument object.

  .PARAMETER Path
  Specifies the file path to the XML file.

  .INPUTS
  None. You cannot pipe objects to Load-XmlFile.

  .OUTPUTS
  System.Xml.XmlDocument. Load-XmlFile returns an XML document.

  .EXAMPLE
  PS> Load-XmlFile 'foo.xml'
#>
Function Load-XmlFile () {
  [OutputType([System.Xml.XmlDocument])]
  Param(
    [Parameter(Mandatory=$true, Position=0)] [System.String] $Path
  )
  Process {
    $xmldoc = New-Object System.Xml.XmlDocument
    $xmldoc.PSBase.PreserveWhitespace = $true
    $xmldoc.Load($Path)
    return $xmldoc
  }
}

﻿$ErrorActionPreference = 'Stop' # stop on all errors

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  softwareName  = 'GLPI Agent *'
  unzipLocation = "$(Get-ToolsLocation)\glpi-agent-nightly"
}

If (Test-Path -Path $packageArgs.unzipLocation) {
  Get-ChildItem -Path $packageArgs.unzipLocation -Depth 1 -Filter "*.bat" `
  | ForEach-Object {
    Uninstall-BinFile -Name $_.Name.Replace(".bat", "")
  }

  Remove-Item -Path "$(Get-ToolsLocation)\glpi-agent-nightly"

  Write-Host "$packageName has been uninstalled."
} Else {
  Write-Warning "$packageName has already been uninstalled by other means."
}
﻿$ErrorActionPreference = 'Stop' # stop on all errors

$toolsDir = "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)"

$packageArgs = @{
  packageName = $env:ChocolateyPackageName
  unzipLocation = "$(Get-ToolsLocation)\glpi-agent-nightly"
  url = 'https://nightly.glpi-project.org/glpi-agent/GLPI-Agent-1.7-git69b99765-x86.zip' # (From $Latest)
  file = 'GLPI-Agent-1.7-git69b99765-x86.zip' # (From $Latest)
  file64 = 'GLPI-Agent-1.7-git69b99765-x64.zip' # (From $Latest)

  softwareName = 'GLPI Agent *'

  checksum = '2BDBCF0A9F6B5DD04074A0590C37F6378A013DB6B1330045BB9316108B0F245D' # (From $Latest)
  checksumType = 'sha256' # (From $Latest)
  checksum64 = 'F84F091A2105C53354A0D020828BD7AF70E8A8AD9DEDCAE261BC033D3D2D96D8' # (From $Latest)
  checksumType64 = 'sha256' # (From $Latest)
}

If ((Get-OSArchitectureWidth 32) -or $env:ChocolateyForceX86 -eq 'true') {
  # Download the 32-bit archive on-the-fly and "install" it
  Install-ChocolateyZipPackage @packageArgs

  # Cleaning
  Remove-Item -Path $packageArgs.file
} Else {
  # Use the embedded 64-bit archive

  # Access file from $toolsDir
  $packageArgs.file64 = Join-Path $toolsDir $packageArgs.file64

  # "Install" the archive
  Get-ChocolateyUnzip @packageArgs

  # Cleaning
  Remove-Item -Path $packageArgs.file64
}

Get-ChildItem -Path $packageArgs.unzipLocation -Depth 1 -Filter "*.bat" `
| ForEach-Object {
  Install-BinFile -Name $_.Name.Replace(".bat", "") -Path $_.VersionInfo.FileName
}

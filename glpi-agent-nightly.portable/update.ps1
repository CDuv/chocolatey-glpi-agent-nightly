﻿<#
  .SYNOPSIS
  Updates the glpi-agent-nightly.portable Chocolatey package.

  .DESCRIPTION
  The update.ps1 script fetches information about the latest GLPI Agent Nightly
  build and updates the .nuspec file of the Chocolatey package accordingly (if
  needed). In such case it builds the .nupkg (`choco pack`).
  You can alter script's behavior by setting:
  * $au_Force to $true to update/package no matter what the script fetched.
  * $au_WhatIf to $true to only simulate changes.
  * $au_SkipBuilds to the number of builds the script should skip (from the
    latest). Defaults to 0.

  .INPUTS
  None. You cannot pipe objects to update.ps1.

  .OUTPUTS
  None. update.ps1 does not generate any output.

  .EXAMPLE
  PS> .\update.ps1

  .EXAMPLE
  PS> $au_WhatIf = $true; .\update.ps1
  # Simulate changes.

  .EXAMPLE
  PS> $au_Force = $au_WhatIf = $true; .\update.ps1
  # Simulate and force changes.
#>

Import-Module au

# Load some global variables
. '..\src\globals.ps1'

# Load functions
. '..\src\functions.ps1'

$softwareTitle = 'GLPI Agent Nightly'
$packageVariantPostfix = ' (Portable, CommandLine)'

<#
  .SYNOPSIS
  Perform pre-update tasks.

  .DESCRIPTION
  Does some cleaning by removing any older archives.
  Fetches software assets (id. archives) into "tools/" and compute their
  checksum, updates the $Latest variable accordingly.
#>
Function global:au_BeforeUpdate {
  Get-RemoteFiles -Purge -NoSuffix -Algorithm $global:checksumAlgorithm

  # Delete the 32-bit archive (because this package only embeds archive for 64-bit)
  $32bitArchiveFilepath = Join-Path -Path (Resolve-Path "tools") -ChildPath $Latest.FileName32
  If (Test-Path -Path $32bitArchiveFilepath -PathType Leaf) {
    Remove-Item -Path $32bitArchiveFilepath -Force
  }
}

<#
  .SYNOPSIS
  Populates $Latest with data taken from the "Nightly Builds" website.

  .DESCRIPTION
  Fetches LICENCE from repository.
  Fetches/scraps assets' URL and version/date from "Nightly Builds" website.
#>
Function global:au_GetLatest {
  # Fetch the "Nightly Builds" page
  $buildsPage = Invoke-WebRequest -Uri $global:buildsUrl -UseBasicParsing

  # Was the script asked to skip builds? $au_SkipBuilds variable set?
  If (-Not (Test-Path variable:global:au_SkipBuilds)) {
    $au_SkipBuilds = 0
  } Else {
    Write-Information -MessageData "Skipping $($au_SkipBuilds) builds…" -InformationAction Continue
  }

  # Get the links for the last archive assets
  $downloadLinkPaths = Get-LastBuiltAssetLinks $buildsPage 'zip' $au_SkipBuilds

  # Extract version string from the link's href
  $officialVersion = Get-OfficialVersionFromAssets $downloadLinkPaths

  # Extract build date and time
  $buildDateAndTime = Get-BuildDateTimeForVersion $buildsPage $officialVersion

  # Expand obtained hrefs to full URLs
  $downloadUrls = $downloadLinkPaths | Foreach-Object { Get-ExpandedHrefOfUri -Href $_.href -Uri ([System.Uri]$global:buildsUrl) | Select-Object -ExpandProperty AbsoluteUri }

  # We need a SemVer compliant version for the Chocolatey Automatic Package
  # Updater Module to correctly detect new versions
  $semverVersion = Get-CompatibleSemVer $officialVersion $buildDateAndTime.Date $buildDateAndTime.Time

  # We need a SemVer compliant version without prerelease part for the Chocolatey
  # Community Repository to think it's a stable release (not a prerelease)
  $semverVersionNotPrelease = Get-CompatibleSemVer -NotPrelease $officialVersion $buildDateAndTime.Date $buildDateAndTime.Time

  # Computes raw URL for the licence and fetch it
  $licenceUrls = Get-LicenceUrlsForGitReference $global:licenceUrl (Get-GitCommitId $semverVersion)
  $licencePage = Invoke-WebRequest -Uri $licenceUrls.Raw -UseBasicParsing

  # Load the .nuspec-ext file to get the "true" semver version
  $nuExt = Load-XmlFile (".\$($Latest.PackageName).nuspec-ext" | Convert-Path)

  # Population for $Latest
  @{
    Version = $semverVersion

    # Force the "NuspecVersion" to make Update-Package's version comparison work
    # by using the semver
    NuspecVersion = $nuExt.package.metadata.semver

    PackageVersion = $semverVersionNotPrelease
    OfficialVersion = $officialVersion
    URL32 = $downloadUrls | Where { $_ -match '-x86.zip$' }
    URL64 = $downloadUrls | Where { $_ -match '-x64.zip$' }
    ChecksumType32 = $global:checksumAlgorithm
    ChecksumType64 = $global:checksumAlgorithm
    BuildDate = $buildDateAndTime.Date
    BuildTime = $buildDateAndTime.Time
    LicenceUrl = $licenceUrls.Pretty
    LicenseRawContent = $licencePage.Content
    LicenceRawUrl = $licenceUrls.Raw
  }
}

<#
  .SYNOPSIS
  Update configuration files with latest values.

  .DESCRIPTION
  Uses $Latest to alter/update install scripts, .nuspec and legal files.
#>
Function global:au_SearchReplace {
  # Make working copy of thoses 2 templates
  Copy-Item '.\legal\LICENSE.tpl' '.\legal\LICENSE.txt' -Force
  Copy-Item '.\legal\VERIFICATION.tpl' '.\legal\VERIFICATION.txt' -Force

  @{
    "$($Latest.PackageName).nuspec" = @{
      '(\<title\>).*?(\</title\>)' = "`${1}$($softwareTitle) v$($Latest.OfficialVersion)$($packageVariantPostfix)`${2}"
      '(\<version\>).*?(\</version\>)' = "`${1}$($Latest.PackageVersion)`${2}"
      '(\<licenseUrl\>).*?(\</licenseUrl\>)' = "`${1}$($Latest.LicenceUrl)`${2}"
      '(\<releaseNotes\>).*?(\</releaseNotes\>)' = "`${1}<![CDATA[This v$($Latest.OfficialVersion) nightly build was built on $($Latest.BuildDate) $($Latest.BuildTime) UTC.]]>`${2}"
    }

    # Update our .nuspec-ext file:
    "$($Latest.PackageName).nuspec-ext" = @{
      '(\<version\>).*?(\</version\>)' = "`${1}$($Latest.PackageVersion)`${2}"
      '(\<semver\>).*?(\</semver\>)' = "`${1}$($Latest.Version)`${2}"
      '(\<softwareOfficialVersion\>).*?(\</softwareOfficialVersion\>)' = "`${1}$($Latest.OfficialVersion)`${2}"
    }

    '.\tools\chocolateyinstall.ps1' = @{
      "(?i)^(\s*url\s*=\s*)'.*'" = "`${1}'$($Latest.URL32)'"
      "(?i)^(\s*file\s*=\s*)'.*'" = "`${1}'$($Latest.FileName32)'"
      "(?i)^(\s*file64\s*=\s*)'.*'" = "`${1}'$($Latest.FileName64)'"
      "(?i)^(\s*checksum\s*=\s*)'.*'" = "`${1}'$($Latest.Checksum32)'"
      "(?i)^(\s*checksum64\s*=\s*)'.*'" = "`${1}'$($Latest.Checksum64)'"
      "(?i)^(\s*checksumType\s*=\s*)'.*'" = "`${1}'$($Latest.ChecksumType32)'"
      "(?i)^(\s*checksumType64\s*=\s*)'.*'" = "`${1}'$($Latest.ChecksumType64)'"
    }

    '.\legal\LICENSE.txt' = @{
      '{{LicenseUrl}}' = $Latest.LicenceRawUrl
      '{{LicenseContent}}' = $Latest.LicenseRawContent
    }

    '.\legal\VERIFICATION.txt' = @{
      '{{PackageName}}' = $Latest.PackageName
      '{{SoftwareTitle}}' = $softwareTitle
      '{{ReleasesUrl}}' = $global:buildsUrl
      '{{OfficialVersion}}' = $Latest.OfficialVersion
      '{{DownloadUrl}}' = $Latest.URL32
      '{{DownloadUrlx64}}' = $Latest.URL64
      '{{ChecksumType}}' = $Latest.ChecksumType32
      '{{ChecksumTypex64}}' = $Latest.ChecksumType64
      '{{Checksum}}' = $Latest.Checksum32
      '{{Checksumx64}}' = $Latest.Checksum64
    }
  }
}

# Starts the actual update of the package but do not automatically compute
# checksum nor update the README.
Update-Package -ChecksumFor none -NoReadme

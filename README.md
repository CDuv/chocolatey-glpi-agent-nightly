GLPI Agent Nightly Chocolatey package
=====================================

This repository contains [*Chocolatey*](https://chocolatey.org) packages for
**nightly builds** of [*GLPI Agent*](https://github.com/glpi-project/glpi-agent).

Theses packages will silently install *GLPI Agent* (an auto-inventory tool for
[*GLPI*](https://glpi-project.org)) on Windows operating systems via
*Chocolatey* Windows Package Manager.

*GLPI* is a Information Resource-Manager. You can use it to build up a database
with an inventory of all your IT assets (computers, softwares, printers,
switches, …). Also includes a ticket-based Helpdesk, planned maintenance tasks,
network topology report.

The *GLPI* inventory can be automatically populated using *GLPI Agent* deployed
on computers to fetch name, hardward specs, network configuration, installed
softwares, …

The repository currently contains the source of 3 *Chocolatey* packages:

* `glpi-agent-nightly.install`: A package that fetches and installs *GLPI Agent*
  into *Windows*.
* `glpi-agent-nightly.portable`: A package that fetches the portable (no
  installation into *Windows*: not added to the *Programs and Features* *Control
  Panel* item) version of the *GLPI Agent*.
* `glpi-agent-nightly`: a virtual package that points to `glpi-agent-nightly.install`.

Packages in this repository are automatically updated to match latest available
nightly build using the [Chocolatey Automatic Package Updater Module](https://github.com/chocolatey-community/chocolatey-au).

Usage
-----

:exclamation: [*Chocolatey*](https://chocolatey.org) is required to use the
`glpi-agent-nightly` Chocolatey packages, see [installation instructions](https://docs.chocolatey.org/en-us/choco/setup).

:grey_exclamation: Cloning of this repository is not required to use the packages:
they are stored on [*Chocolatey Community Repository* (*CCR*)](https://docs.chocolatey.org/en-us/community-repository/):

* [`glpi-agent-nightly.install`](https://community.chocolatey.org/packages/glpi-agent-nightly.install/)
* [`glpi-agent-nightly.portable`](https://community.chocolatey.org/packages/glpi-agent-nightly.portable/)
* [`glpi-agent-nightly`](https://community.chocolatey.org/packages/glpi-agent-nightly/)

### Examples

* Install the latest version of the agent (runs installer with defaults settings):

  ```Batchfile
  choco install glpi-agent-nightly
  ```

* Install a specific version of the agent (runs installer with defaults settings):

  ```Batchfile
  choco update glpi-agent-nightly --version=1.5-nightly-20230429-022216+git6b8b966f
  ```

* Update already installed agent with latest version:

  ```Batchfile
  choco update glpi-agent-nightly
  ```

* Run agent installer with custom settings by providing the installer some
  command line options (via Chocolatey `--install-args` option, [see
  documentation for the Chocolatey `install` action](https://docs.chocolatey.org/en-us/choco/commands/install#options-and-switches)
  for more options):

  * Set the URL of the GLPI server to use, don't inventory printer, configure to
    run every 6 hours but perform the first run/contact with the server within 5
    minutes after the installation:

    ```Batchfile
    choco install glpi-agent-nightly --install-args "/server='https://glpi.example.com/front/inventory.php' NO_CATEGORY=Printer TASK_FREQUENCY=hourly TASK_HOURLY_MODIFIER=6 DELAYTIME=300"
    ```

  * Set the URL of the GLPI server to use, don't install as a service but as a
    Windows Schedule Task scheduled to run every 2 days but perform the first
    run/contact with the server immediately after installation:

    ```Batchfile
    choco install glpi-agent-nightly --install-args "/server='https://glpi.example.com/front/inventory.php' EXECMODE=Task TASK_FREQUENCY=daily TASK_HOURLY_MODIFIER=2 RUNNOW=1"
    ```

  * PowerShell version that uses machine's domain (Windows AD) to set the HTTP
    proxy the agent should use:

    ```PowerShell
    choco install glpi-agent-nightly --install-args "PROXY='http://proxy.$env:userdnsdomain'"
    ```

  The list of available command line options can be found on the [*GLPI Agent*
  documentation of the *Windows installer*](https://glpi-agent.readthedocs.io/en/latest/installation/windows-command-line.html).

Packaging a new version
-----------------------

The [Chocolatey Automatic Package Updater Module](https://github.com/chocolatey-community/chocolatey-au)
is used to automatically update theses packages.

Create a package for the latest available version:

```PowerShell
Set-ExecutionPolicy Bypass -Scope Process -Force
.\update.ps1
```

Create a package for the before last available version:

```PowerShell
Set-ExecutionPolicy Bypass -Scope Process -Force
$au_SkipBuilds = 1 ; .\update.ps1
```

Only simulate creation of a new package:

```PowerShell
Set-ExecutionPolicy Bypass -Scope Process -Force
$au_WhatIf = $true ; .\update.ps1
```

Force creation of a new package:

```PowerShell
Set-ExecutionPolicy Bypass -Scope Process -Force
$au_Force = $true ; .\update.ps1
```

Credits
-------

*GLPI Agent* installers, portable archives, documentation and the logo belongs
(-ish) to GLPI Agent's owner and contributors.

The packaging scripting material of this repository is not affiliated to GLPI's
owners and contributors.
